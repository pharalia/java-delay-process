package oldruk;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.*;

class BasicScheduledThreadExecutorTest {
    private static final long DELAY_TIME = 1_000_000L;

    ScheduledExecutorService scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);

    @Test
    void testADelayedExecution() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        scheduledThreadPoolExecutor.schedule(
                countDownLatch::countDown,
                DELAY_TIME,
                TimeUnit.NANOSECONDS
        );

        LocalDateTime start = LocalDateTime.now();
        countDownLatch.await(2L, TimeUnit.SECONDS);
        LocalDateTime finish = LocalDateTime.now();

        long duration = Duration.between(start, finish).toNanos();

        MatcherAssert.assertThat(duration, Matchers.greaterThanOrEqualTo(DELAY_TIME));
        System.out.println(duration);
    }
}
