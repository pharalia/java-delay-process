# Java Delay Process
An experiment in testing delayed processing with `ScheduledThreadPoolExecutor`

To run, excitedly run;

```bash
$ ./gradlew test
```

or 

```
> gradlew.bat test
```

See [Implementing and Testing Delayed Processing in Java](https://www.michaeloldroyd.co.uk/posts/testing-futures-in-java/)